package com.ushakov.tm.api.repository;

import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String arg);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    List<String> getCommandNames();

    @NotNull
    List<AbstractCommand> getCommandsWithArguments();

    @NotNull
    List<AbstractCommand> getCommands();

}
