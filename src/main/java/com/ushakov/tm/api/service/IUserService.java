package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@Nullable String login, @Nullable String password);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findOneByLogin(@Nullable String login);

    @NotNull
    User removeOneByLogin(@Nullable String login);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, String middleName);

    @NotNull
    User lockByLogin(@Nullable String login);

    @NotNull
    User unlockByLogin(@Nullable String login);

}
