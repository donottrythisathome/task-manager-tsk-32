package com.ushakov.tm.api.service;

import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project deleteProjectById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<Task> findAllTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

}
