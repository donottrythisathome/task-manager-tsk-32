package com.ushakov.tm.model;

import com.ushakov.tm.api.model.IWBS;
import com.ushakov.tm.enumerated.Status;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractOwnerBusinessEntity extends AbstractOwnerEntity implements IWBS {

    @NotNull
    protected String name = "";

    @Nullable
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date dateStart;

    @Nullable
    protected Date dateEnd;

    @NotNull
    protected Date created = new Date();

}