package com.ushakov.tm.component;

import com.ushakov.tm.bootstrap.Bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30;

    private static final String BACKUP_SAVE = "backup-save";

    private static final String BACKUP_LOAD = "backup-load";

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

}
