package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Error! Domain is empty!");
    }

}
