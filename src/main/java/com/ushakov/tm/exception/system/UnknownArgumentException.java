package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull String value) {
        super("Error! Unknown argument: " + value + "!");
    }

}
