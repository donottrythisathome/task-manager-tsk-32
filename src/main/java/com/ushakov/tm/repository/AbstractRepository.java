package com.ushakov.tm.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    @NotNull
    public E add(@NotNull final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        list.addAll(entities);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return list;
    }

    @Override
    @Nullable
    public E findOneById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @Override
    @Nullable
    public E removeOneById(@NotNull final String id) {
        @Nullable final E entity = findOneById(id);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}
